import Link from "next/link";
import PostLink from "./PostLink";

export default class PostList extends React.Component {
  constructor(props) {
    super(props);

    this.state = { posts: this.props.posts || [] };
  }

  render() {
    if (this.state.posts.length == 0) return <div>No posts available.</div>;

    const listBody = this.state.posts.map((post, index) => (
      <li key={index}>
        <PostLink title={post} href={`/post?title=${post}`} />
      </li>
    ));

    return <ul>{listBody}</ul>;
  }
}
