import Link from "next/link";

const PostLink = props => (
  <Link as={`/p/${props.title}`} href={`/post?title=${props.title}`}>
    <a title={props.title}>Post: {props.title}</a>
  </Link>
);

export default PostLink;
