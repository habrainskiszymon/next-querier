import theme from "../../theme";

class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.title,
      author: this.props.author,
      content: this.props.children
    };
  }

  render() {
    const { title, author, content } = this.state;

    return (
      <article>
        <h1>{title}</h1>
        {author ? (
          <p className="author-info">
            <span>written by: </span>
            {author}
          </p>
        ) : null}
        <div className="post-content">{content}</div>
        <style jsx global>
          {`
            .post-content {
              color: blue;
              font-size: 1rem;
              background-color: ${theme["post-background-color"]};
            }
          `}
        </style>
        <style jsx>{`
          .author-info span {
            font-style: italic;
          }
        `}</style>
      </article>
    );
  }
}

export default Post;
