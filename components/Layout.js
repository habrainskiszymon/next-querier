import Header from "./Header";
import Head from "next/head";
// import css from "styled-jsx/css";

class Layout extends React.Component {
  render() {
    return (
      <div>
        <Head>
          <title>{this.props.title}</title>
        </Head>
        <Header title={this.props.title} />
        <main>{this.props.children}</main>
        <style jsx>{`
          main {
            margin: 1em 0 0 0;
            border: 1px solid red;
          }
        `}</style>
      </div>
    );
  }
}

export default Layout;
