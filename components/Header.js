import Link from "next/link";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <header>
          <h1>{this.props.title}</h1>
          <nav>
            <Link href="/">
              <a title="Home">Home</a>
            </Link>
            <Link href="/about">
              <a title="About">About</a>
            </Link>
            <Link href="/wiki">
              <a title="Wiki">Wiki</a>
            </Link>
          </nav>
        </header>
        <style jsx>
          {`
            nav {
              display: flex;
              justify-content: center;
              border: 1px solid gray;
            }

            nav a {
              border: 1px solid gray;
              margin: 0 1em 0 0;
              padding: 1em;
            }

            nav a:last-child {
              margin: 0;
            }

            nav a:hover {
              background-color: #cccccc;
            }
          `}
        </style>
      </div>
    );
  }
}

export default Header;
