import Layout from "../components/Layout";
import Post from "../components/Post/Post";
import { withRouter } from "next/router";
import fetch from "isomorphic-unfetch";
import ReactHtmlParser from "react-html-parser";

function fetchPostWithTitle(title) {
  if (!title) return null;
  const db = [
    {
      title: "Mamamia",
      author: "Szymon Habrainski",
      content: (
        <div>
          Pariatur ad ut culpa eiusmod consectetur incididunt proident occaecat.
          Anim aute labore veniam irure aute proident. Enim eiusmod fugiat
          labore proident. Aliquip qui dolore veniam ut nisi sunt occaecat
          exercitation aliquip cillum incididunt commodo sit. Labore minim
          excepteur cupidatat officia sint culpa non exercitation veniam duis
          laboris eu ad.
        </div>
      )
    },
    {
      title: "Loosie",
      author: "Julia Wagner",
      content: <div>This is a loosie blog post.</div>
    },
    {
      title: "Loosie",
      author: "Bernd Schnösel",
      content: <div>Als Reaktion auf Julias Artikel...</div>
    },
    {
      title: "Loosie",
      author: "Bernd Schnösel",
      content: <div>Als Reaktion auf Julias Artikel...</div>
    }
  ];

  const result = db.filter(post => post.title == title);

  return result.length == 0 ? null : result;
}

const PostRouter = withRouter(
  class extends React.Component {
    static async getInitialProps(context) {
      const title = context.query.title;

      const res = await fetch(
        `https://en.wikipedia.org/w/api.php?action=query&list=search&origin=*&format=json&srlimit=10&srsearch=${title}`
      );

      let data;
      let posts = [];

      try {
        data = await res.json();

        posts = data.query.search.map(hit => ({
          title: hit.title,
          content: hit.snippet
        }));
      } catch (e) {
        console.error(data);
      }

      return {
        posts: posts
      };
    }

    render() {
      const { posts } = this.props;

      if (!posts)
        return (
          <p>A post named '{this.props.router.query.title}' does not exist.</p>
        );

      return (
        <Layout title={`A Blog Post: ${this.props.router.query.title} `}>
          {posts.map((post, index) => (
            <Post key={index} title={post.title} author={post.author}>
              {ReactHtmlParser(post.content)}...
            </Post>
          ))}
        </Layout>
      );
    }
  }
);

export default PostRouter;
