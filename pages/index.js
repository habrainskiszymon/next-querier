import Layout from "../components/Layout";
import Post from "../components/Post/Post";

import Link from "next/link";

const Index = () => (
  <div>
    <Layout title="Index Page">
      <Post title="My first Post" author="Szymon Habrainski">
        <h1>Hello</h1>
        <p>I am writing my post right now...</p>
      </Post>
    </Layout>
  </div>
);

export default Index;
