import Layout from "../components/Layout";
import PostList from "../components/Post/PostList";
import fetch from "isomorphic-unfetch";

import React, { Component } from "react";

const About = () => (
  <div>
    <Layout title="About Page">
      <PostList posts={["Batman"]} />
    </Layout>
  </div>
);

export default About;
