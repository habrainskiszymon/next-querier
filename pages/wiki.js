import React, { Component } from "react";
import Layout from "../components/Layout";
import fetch from "isomorphic-unfetch";
import ReactHtmlParser from "react-html-parser";

export default class Wiki extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prevQuery: "",
      query: "",
      results: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    e.persist();
    this.setState(state => ({
      query: e.target.value
    }));
  }

  handleSubmit(e) {
    e.preventDefault();

    if (this.state.query === "") return;
    const query = this.state.query;

    fetch(
      `https://en.wikipedia.org/w/api.php?action=query&list=search&origin=*&format=json&srlimit=10&srsearch=${query}`
    )
      .then(searchResult => {
        return searchResult.json();
      })
      .then(searchResult => {
        const pageDetailSet = searchResult.query.search.map(pageDetails => ({
          pageid: pageDetails.pageid,
          title: pageDetails.title,
          snippet: pageDetails.snippet
        }));

        const pageIds = pageDetailSet
          .map(pageDetails => pageDetails.pageid)
          .join("|");

        return fetch(
          `https://en.wikipedia.org/w/api.php?origin=*&action=query&pithumbsize=500&prop=pageimages&format=json&pageids=${pageIds}`
        )
          .then(thumbnailResults => thumbnailResults.json())
          .then(thumbnailResults => {
            return {
              thumbnailResults: thumbnailResults,
              pageDetailSet: pageDetailSet
            };
          });
      })
      .then(({ thumbnailResults, pageDetailSet }) => {
        return pageDetailSet.map(pageDetails => {
          if (thumbnailResults.query.pages[pageDetails.pageid].thumbnail) {
            return {
              ...pageDetails,
              thumbnail:
                thumbnailResults.query.pages[pageDetails.pageid].thumbnail
            };
          }

          return {
            ...pageDetails,
            thumbnail: null
          };
        });
      })
      .then(pageDetailSet => {
        console.log(pageDetailSet);
        this.setState({ results: pageDetailSet });
      });
  }

  getResults() {
    if (this.state.results.length > 0) {
      return (
        <div className="entry-wrapper">
          {this.state.results.map(result => (
            <div key={result.pageid} className="entry">
              <h3>{result.title}</h3>
              <code>Page ID: {result.pageid}</code>
              <p>{ReactHtmlParser(result.snippet)}...</p>
              {result.thumbnail ? (
                <img
                  src={result.thumbnail.source}
                  width={result.thumbnail.width}
                  height={result.thumbnail.height}
                />
              ) : null}
            </div>
          ))}
          <style jsx>{`
            .entry {
              padding: 1em;
              background-color: #eeeeee;
              margin: 0 0 1em 0;
            }

            .entry:last-child {
              margin: 0;
            }
          `}</style>
        </div>
      );
    } else {
      return <div>I am waitin' for ya...</div>;
    }
  }

  render() {
    return (
      <Layout title="Wiki">
        <div>
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="query-input">
              <h1>Your Query:</h1>
            </label>
            <input
              value={this.state.query}
              name="query-input"
              type="text"
              onChange={this.handleChange}
            />
          </form>
          <section>{this.getResults()}</section>
        </div>
        <style jsx>{`
          form {
            display: flex;
            align-items: center;
            flex-direction: column;
          }
          input[name="query-input"] {
            font-size: 1.5rem;
            width: 20em;
            height: 1.5rem;
            margin: 0 0 1em 0;
          }
          section {
            font-family: Helvetica;
          }
        `}</style>
      </Layout>
    );
  }
}
